# Project Dependencies

This project requires the following tools and libraries:

## Tools
- A C++ Compiler supporting C++11.
- CMake (version 3.1 or later)

## Libraries
- yaml-cpp

## Package Managers (optional)
- If you are on Ubuntu, `apt` can be used to install yaml-cpp (`sudo apt-get install libyaml-cpp-dev`).
- If you are on MacOS, `brew` can be used to install yaml-cpp (`brew install yaml-cpp`).

Please make sure all of these are installed and properly configured before building the project.
